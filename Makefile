# x11-dpms-hack makefile

ifndef prefix
# This little trick ensures that make install will succeed both for a local
# user and for root. It will also succeed for distro installs as long as
# prefix is set by the builder.
prefix=$(shell perl -e 'if($$< == 0 or $$> == 0) { print "/usr" } else { print "$$ENV{HOME}/.local"}')

# Some additional magic here, what it does is set BINDIR to ~/bin IF we're not
# root AND ~/bin exists, if either of these checks fail, then it falls back to
# the standard $(prefix)/bin. This is also inside ifndef prefix, so if a
# prefix is supplied (for instance meaning this is a packaging), we won't run
# this at all
BINDIR ?= $(shell perl -e 'if(($$< > 0 && $$> > 0) and -e "$$ENV{HOME}/bin") { print "$$ENV{HOME}/bin";exit; } else { print "$(prefix)/bin"}')
endif

BINDIR ?= $(prefix)/bin
DATADIR ?= $(prefix)/share
DISTFILES=x11-dpms-hack README.md COPYING.md
VERSION=$(shell ./x11-dpms-hack --version|perl -p -e 's/^x11\D+//; chomp')

# Install x11-dpms-hack
install:
	mkdir -p "$(BINDIR)"
	cp x11-dpms-hack "$(BINDIR)"
	chmod 755 "$(BINDIR)/x11-dpms-hack"
localinstall:
	mkdir -p "$(BINDIR)"
	ln -sf $(shell pwd)/x11-dpms-hack $(BINDIR)/
# Uninstall an installed x11-dpms-hack
uninstall:
	rm -f "$(BINDIR)/x11-dpms-hack"
# Verify syntax
sanity:
	@perl -c x11-dpms-hack
# Create the tarball
distrib: sanity
	mkdir -p x11-dpms-hack-$(VERSION)
	cp -r $(DISTFILES) ./x11-dpms-hack-$(VERSION)
	tar -jcvf x11-dpms-hack-$(VERSION).tar.bz2 ./x11-dpms-hack-$(VERSION)
	rm -rf x11-dpms-hack-$(VERSION)
clean:
	rm -f *~ *.bak *.tar.bz2
# Format with perltidy
format:
	perltidy -bl -b x11-dpms-hack
# Update the README
updateReadme:
	./x11-dpms-hack --help > readme.out
	perl ./.update-readme.pl ./README.md ./readme.out
	rm -f readme.out
