# x11-dpms-hack

Is your screen not blanking when it should under X11 (perhaps using the Nvidia
driver and GNOME)? This is a very ugly hack that fixes that. It queries the X11
idletime (and, optionally, gamemode status), and if idle time exceeds a set
number of seconds, it forces the screen off.

It is fairly lightweight, and auto-nices itself, so it should not put any
strain on your system, while letting your screen actually blank.

This doesn't actually *fix* any of the underlying issues, but works around them
so that you can get on with your day.

By default x11-dpms-hack will daemonize and get out of your way as soon as you
start it. Note that it does not check for multiple copies of itself, that is up
to you.

You will want to add it to autostart. It can do this for you using
`x11-dpms-hack --autostart` (you can combine this with other options, like
`--gamemode` to have those options applied during autostart).

## Examples

`x11-dpms-hack`  
Simply start the daemon with defaults

`x11-dpms-hack --gamemode`  
Start the daemon with game mode integration

`x11-dpms-hack --no-daemon`  
Start the hack but do not go into the background

`x11-dpms-hack --verbose`  
Start the hack with verbose mode and do not go into the background

## Options

```
Usage: x11-dpms-hack [options]

     --idle-time SECONDS         Require the user to have been idle (no input) for SECONDS
                                 before turning off the screen. Default is 900s (15m)
     --loop-time SECONDS         Check for idle status every SECONDS. Default is 300s
                                 (5m)
     --gamemode                  Enable gamemode integration, will not blank the screen
                                 if any applications are using gamemode
     --autostart                 Enable autostart of x11-dpms-hack. It will be enabled with
                                 the current options so add ie. --gamemode as well
                                 if you want that to be enabled on startup.
     --query                     Query state. x11-dpms-hack will exit with 1 if it thinks
                                 the computer is in active use (and would not turn off
                                 the display), 0 if it thinks it is idle.
 -v, --verbose                   Be verbose
     --version                   Output version number and exit.
```

## License

x11-dpms-hack is Copyright © Eskild Hustvedt 2023

x11-dpms-hack is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

x11-dpms-hack is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see https://www.gnu.org/licenses/gpl-3.0.txt.
