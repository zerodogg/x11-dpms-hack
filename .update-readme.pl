#!/usr/bin/perl
use 5.036;
use IO::All;

my $inputFile   = shift(@ARGV);
my $includeFile = shift(@ARGV);

my @markdown       = io($inputFile)->slurp;
my $includeContent = io($includeFile)->slurp;

my $seenIncludeStatement = 0;
my $seenEndStatement     = 0;

my @out;

foreach my $line (@markdown) {
    if ($seenIncludeStatement) {
        if ( !$seenEndStatement ) {
            if ( $line =~ /^```/ ) {
                die("Saw multiple ``` end statements") if $seenEndStatement;
                $seenEndStatement++;
            }
            else {
                next;
            }
        }
    }
    push( @out, $line );
    if ( $line =~ /^```/ && !$seenEndStatement ) {
        $seenIncludeStatement = 1;
        push( @out, $includeContent );
    }
}

die("Saw no ending statement") if !$seenEndStatement;

io($inputFile)->write( join( '', @out ) );
